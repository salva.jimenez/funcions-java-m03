/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.exemples_funcions;

/**
 *
 * @author salva
 */
public class funcio_invertirNumero {
    public static void main(String[] args) {
        int numero2 = 32; //long2 23
        int numero3 = 345; //long3 543
        int numero4 = 3458; //long4 8543
        
        System.out.println("El numero invertit de " + numero4 + " és " + invertirDigitsNumero(numero4));
    }
    
    public static int invertirDigitsNumero(int numero) {
        int numeroInvertit = 0;
        int ultimDigit;
        
        while (numero != 0) {
            ultimDigit = numero % 10;
            numeroInvertit = numeroInvertit * 10 + ultimDigit;
            numero = numero/10;
        }
        return numeroInvertit;
    }
}
