package com.mycompany.exemples_funcions;

import java.util.Random;

/**
 *
 * @author salva
 */
public class functio_generarArray2D {

    public static void main(String[] args) {
        final int TAMANY = 9;
        final int MIN_NUM = 1, MAX_NUM = 9;

        int[][] array2D;

        array2D = generarArray2D(TAMANY, MIN_NUM, MAX_NUM);
        imprimirArray2D(array2D);
        
    }

    public static int[][] generarArray2D(int tamany, int min_num, int max_num) {
        int[][] array = new int[tamany][tamany];
        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = random.nextInt(max_num) + min_num;
            }
        }
        return array;
    }

    public static void imprimirArray2D(int[][] array2D) {
        for (int i = 0; i < array2D.length; i++) {
            for (int j = 0; j < array2D[i].length; j++) {
                System.out.print(array2D[i][j] + " ");
            }
            System.out.println();
        }
    }

}
