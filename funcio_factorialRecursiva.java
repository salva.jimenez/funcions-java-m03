package com.mycompany.exemples_funcions;

/**
 *
 * @author salva
 */
public class funcio_factorialRecursiva {
    public static void main(String[] args) {
        int numero=5;
        int numero_factorial = factorialRec(numero);
        System.out.println("El factorial de " + numero + " és " + numero_factorial);
    }
    
    //factorial n: n! = n*(n-1)*(n-2)*...*1 
    //o         n! = n*(n-1)!
    public static int factorialRec(int numero) {
        if (numero < 1) 
            return 0;
        
        if (numero == 1) {
            return 1;
        } else {
            return numero * factorialRec(numero-1);
        }
        
    }
 
}
