package com.mycompany.exemples_funcions;

/**
 *
 * @author salva
 */
public class funcio_imprimirArray2D {
    public static void main(String[] args) {
        int[][] array2D = {{2, 4, 1}, {5, 9, 5}, {34, 1, 73}};
        imprimirArray2D(array2D);
    }
    
    public static void imprimirArray2D(int[][] array2D) {
        for (int i = 0; i < array2D.length; i++) {
            for (int j = 0; j < array2D[i].length; j++) {
                System.out.print(array2D[i][j] + " ");
            }
            System.out.println();
        }
    }
}
