/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.exemples_funcions;

import java.util.Arrays;

/**
 *
 * @author salva
 */
public class funcio_imprimirArray {
    public static void main(String[] args) {
        int[] array = {1, 2, 4, 5};
        imprimirArrayNumeric(array);
        System.out.println();
        imprimirArrayNumeric2(array);
    }
    
    public static void imprimirArrayNumeric(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }
    
    public static void imprimirArrayNumeric2(int[] array) {
        String arrayString = Arrays.toString(array);
        System.out.print(arrayString);
        
    }
}
