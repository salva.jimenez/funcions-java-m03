package com.mycompany.exemples_funcions;

/**
 *
 * @author salva
 */
public class funcio_imprimirFibonacci {

    public static void main(String[] args) {
        for (int i = 0; i < 20; i++) {
            //System.out.print(imprimirFibonacci(i)+ " ");
            System.out.print(imprimirFibonnaciRecursiu(i)+ " ");
        }
    }

    public static int imprimirFibonacci(int poscicioFibonacci) {
        int numFibonacci = 0, posFibonacci1, posFibonacci2;
        posFibonacci1 = 0;
        posFibonacci2 = 1;

        if (poscicioFibonacci == 0 || poscicioFibonacci == 1) 
            return poscicioFibonacci;

        for (int i = 0; i <= poscicioFibonacci; i++) {
            numFibonacci = posFibonacci1 + posFibonacci2;
            posFibonacci1 = posFibonacci2;
            posFibonacci2 = numFibonacci;
        }

        return numFibonacci;
    }
    
    public static int imprimirFibonnaciRecursiu(int posicioFibonacci) {
        if (posicioFibonacci == 0 || posicioFibonacci == 1) {
            return posicioFibonacci;
        } else {
            return imprimirFibonnaciRecursiu(posicioFibonacci - 1 ) + 
                   imprimirFibonnaciRecursiu(posicioFibonacci - 2 );
        }
    }
}
