package com.mycompany.exemples_funcions;

public class funcio_esParellRecursiu {
    public static void main(String[] args) {
        int numero = 4;
        System.out.println("El numero " + numero + " es parell? " + esParellRecursiu(numero));
    }
    
    public static boolean esParellRecursiu(int numero) {
        if (numero == 0) 
            return true;
        else
            return esImparellRecursiu(numero - 1);
    }
    
    public static boolean esImparellRecursiu(int numero) {
        if (numero == 0) 
            return false;
        else
            return esParellRecursiu(numero - 1);
    }
}
