package com.mycompany.exemples_funcions;

/**
 *
 * @author salva
 */
public class funcio_imprimirQuadrats {

    public static void main(String[] args) {
        int numero1 = 1, numero2 = 100;
        System.out.println("Els quadrats entre " + numero1 + " i " + numero2 + " són :");
        imprimirQuadrats(numero1, numero2);
    }

    public static void imprimirQuadrats(int num_inici, int num_final) {
        for (int i = num_inici; i <= num_final; i++) {
            System.out.print(i * i + ", ");
            
            if (i % 10 == 0) {
                System.out.println();
            }
        }
    }
}