/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.exemples_funcions;

/**
 *
 * @author salva
 */
public class funcio_ordenarArray {

    public static void main(String[] args) {
        int[] arrayEnters = {1, 5, 2, 6, 3, 10, 4};
        ordenarArray(arrayEnters);

        for (int i = 0; i < arrayEnters.length; i++) {
            System.out.print(arrayEnters[i] + " ");
        }
        System.out.println();
        
    }
    
    public static void ordenarArray(int[] array) {
        int aux;
        
        for (int i = 0; i < array.length; i++) {
            for (int j = i; j < array.length; j++) {
                if (array[j] < array[i]) {
                    aux = array[i];
                    array[i] = array[j];
                    array[j] = aux;
                }
            }
        }
    }
}
