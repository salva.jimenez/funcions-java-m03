/*
 * Un número de n digits és Armstrong si és igual a la suma de 
 * les n-potencies dels seus digits
 * Exemple:
 */
package com.mycompany.exemples_funcions;

/**
 *
 * @author salva
 */
public class funcio_numeroAlRang {
    public static void main(String[] args) {
        int numero = 2;
        int num_inicial = 0;
        int num_final = 5;
        System.out.println("El numero " + numero + " és al rang " + num_inicial + "-" + num_final + "-> " + esAlRang(numero, num_inicial, num_final));
    }
    
    public static boolean esAlRang(int numero, int num_inicial, int num_final) {
        boolean esRang = false;
        
        if (numero >= num_inicial && numero <= num_final)
            esRang = true;
        
        return esRang;
    }
}
