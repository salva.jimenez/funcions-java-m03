package com.mycompany.exemples_funcions;

/**
 *
 * @author salva
 */
public class funcio_palindroma {
    public static void main(String[] args) {
        String palindrom = "Amar da drama";
        System.out.println();
        System.out.println("La frase\n" + palindrom + "\nés un anagrama?: " + esAnagrama(palindrom));

    }
    
    public static boolean esAnagrama(String paraula) {
        boolean esAnagrama = true;
        
        paraula = paraula.replaceAll("\\s", "");
        paraula = paraula.toLowerCase();
        char[] chars = paraula.toCharArray();
        
        for (int i = 0; i < (chars.length - 1) / 2; i++) {
            if (chars[i] != chars[chars.length- i - 1])
                esAnagrama = false;
        }
        return esAnagrama;
    }
}
