package com.mycompany.exemples_funcions;
import java.util.Scanner;

/**
 *
 * @author salva
 */
public class funcio_esPrimer {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numero;
        
        System.out.println("Introdueix un numero");
        numero = scanner.nextInt();
        
        if (esPrimer(numero))
            System.out.println("Aquest número és primer");
        else
            System.out.println("Aquest número no és primer");
    }
    
    /**
     * 
     * @param numero
     * @return 
     */
    public static boolean esPrimer(int numero) {
        boolean primer = true;
        
        for (int i = numero-1; i > 1; i--) {
            if (numero % i == 0) {
                primer = false;
            }
        }
        return primer;
    }
}
