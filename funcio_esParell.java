package com.mycompany.exemples_funcions;

/**
 *
 * @author salva
 */
public class funcio_esParell {
    public static void main(String[] args) {
        int numero = 12;
        System.out.println("El número " + numero + " és parell? " + esParell(numero));
    }
    
    public static boolean esParell(int numero) {
        boolean parell = false;
        
        if (numero % 2 == 0)
            parell = true;
        
        return parell;
    }
    
    public static boolean esParell2(int numero) {
        return numero % 2 == 0;
    }
}
