/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.exemples_funcions;

/**
 *
 * @author salva
 */
public class funcio_max2numeros {
    public static void main(String[] args) {
        int a = 5;
        int b = 0;
        
        System.out.println("El maxim de " + a + " i " + b + " és: " + maxim(a, b));
    }
    
    public static int maxim(int a, int b) {
        int maxim = a;
        
        if (b > maxim) {
            maxim = b;
        }
        
        return maxim;
    }
}
