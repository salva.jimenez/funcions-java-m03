package com.mycompany.exemples_funcions;

/**
 *
 * @author salva
 */
public class funcio_esTraspas {

    public static void main(String[] args) {
        final int anyInicial = 1900;
        final int anyFinal = 2030;

        for (int i = anyInicial; i <= anyFinal; i++) {
            if (esAnyTraspas(i))
                System.out.println("L\'any " + i + " és de traspàs");
        }
    }

    public static boolean esAnyTraspas(int any) {
        boolean esTraspas = false;

        if (((any % 4 == 0) && (any % 100 != 0)) || (any % 400 == 0)) {
            esTraspas = true;
        }

        return esTraspas;
    }
}
