package com.mycompany.exemples_funcions;

/**
 *
 * @author salva
 */
public class funcio_nPrimer {

    public static void main(String[] args) {
        for (int i = 1; i <= 100; i++) {
            if (esPrimer(i)) {
                System.out.println(" El número " + i + " és el " + nPrimer(i) + " numero primer.");
            }
        }
    }

    public static int nPrimer(int numero) {
        int nPrimer = 0;
        
        if (esPrimer(numero)) {
            for (int i = 2; i <= numero; i++) {
                if (esPrimer(i)) {
                    nPrimer++;
                }
            }
        }
        return nPrimer;
    }

    public static boolean esPrimer(int numero) {
        boolean primer = true;
        int i;

        if (numero < 2) {
            primer = false;
        } else {
            i = 2;
            while (primer && i < numero) {
                if (numero % i == 0) {
                    primer = false;
                }
                i++;
            }
        }
        return primer;
    }
}
