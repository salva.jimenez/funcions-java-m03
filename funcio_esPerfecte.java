/*
* Un numero és perfecte és un número positiu que és igual a la suma de tots els divisors 
* del número excepte el número mateix.
* Exemple: 6. Divisors: 1+2+3=6 6==6
* Exemple: 28 Divisors: 1+2+3+4+7+14=28 .  
*/
package com.mycompany.exemples_funcions;

/**
 *
 * @author salva
 */
public class funcio_esPerfecte {
    public static void main(String[] args) {
        
        for (int i = 1; i < 1000000; i++) {
            if (esPerfecte(i))
                System.out.println("El numero " + i + " és pefecte.");
        }
    }
    
    public static boolean esPerfecte(long numero) {
        long suma = 0;
        
        for(int i=1; i<numero; i++) {
            if (numero % i == 0) {
                suma = suma + i;
            }
        }
        return (suma == numero);
    }
}
