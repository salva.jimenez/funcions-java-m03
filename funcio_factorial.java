/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.exemples_funcions;

/**
 *
 * @author salva
 */
public class funcio_factorial {
    public static void main(String[] args) {
        int numero=5;
        int numero_factorial = factorial2(numero);
        System.out.println("El factorial de " + numero + " és " + numero_factorial);
    }
    
    public static int factorial(int numero) {
        int factorial = numero;
        
        for (int i = numero-1; i >= 1; i--) {
            factorial = factorial * i;
        }
        return factorial;
    }
    
    public static int factorial2(int numero) {
        int factorial = 1;
        
        for (int i = 2; i <= numero; i++) {
            factorial = factorial * i;
        }
        return factorial;
    }
}
