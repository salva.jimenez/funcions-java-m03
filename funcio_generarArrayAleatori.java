package com.mycompany.exemples_funcions;
import java.util.Random;

/**
 *
 * @author salva
 */
public class funcio_generarArrayAleatori {

    public static void main(String[] args) {

    }

    public static int[] generarArrayAleatori(int size, int maxRandom) {
        Random random = new Random();
        int[] arrayAleatori = new int[size];

        for (int i = 0; i < arrayAleatori.length - 1; i++) {
            arrayAleatori[i] = random.nextInt(maxRandom);
        }
        
        return arrayAleatori;
    }
}
